from django.shortcuts import render
from django.db.models import Max
from crud.forms import createChatForm
from django.http import HttpResponseRedirect
from crud.models import Chat, Action, Vaccin, Soin, Veterinaire
import datetime


def createChatAction(request):
    if request.method == 'POST':
        form = createChatForm(request.POST)
        if form.is_valid():
            chat = Chat()
            chat.prenom = request.POST['prenom']
            chat.sexe = request.POST['sexe']
            dateNaissance = request.POST['date_naissance']
            chat.date_naissance = datetime.datetime.strptime(dateNaissance, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
            entreeLe = request.POST['entree_le']
            chat.entree_le = datetime.datetime.strptime(entreeLe, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
            chat.identification = request.POST['identification']
            if chat.identification == '2':
                chat.n_puce = request.POST['number']
            elif chat.identification == '3':
                chat.n_tatouage = request.POST['number']

            if 'sterilise' in request.POST:
                chat.sterilise = True
                date_steri = request.POST['date_steri']
                chat.date_steri = datetime.datetime.strptime(date_steri, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
            else:
                chat.sterilise = False
            chat.test_vih = request.POST['test_vih']
            if chat.test_vih != "0":
                date_test_vih = request.POST['date_test_vih']
                chat.date_test_vih = datetime.datetime.strptime(date_test_vih, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
            if 'adopte' in request.POST:
                chat.adopte = True
            else:
                chat.adopte = False
            chat.nom_fa = request.POST['nom_fa']
            chat.nom_adoptant = request.POST['nom_adoptant']
            chat.commentaire = request.POST['commentaire']
            chat.save()
            soin_num = 0
            vaccin_num = 0
            while 'date_vaccin_' + str(vaccin_num) in request.POST:
                dateVaccin = request.POST['date_vaccin_' + str(vaccin_num)]
                vaccin = Vaccin()
                vaccin.date_action = datetime.datetime.strptime(dateVaccin, "%d/%m/%Y") \
                    .strftime("%Y-%m-%d")
                vetId = request.POST['vaccin_veto_name_' + str(vaccin_num)]
                vaccin.veterinaire = Veterinaire.objects.get(pk=vetId)
                vaccin.chat = chat
                vaccin.type_vaccin = vaccin_num
                vaccin.save()
                vaccin_num = vaccin_num + 1
            while 'date_soin_' + str(soin_num) in request.POST:
                dateSoin= request.POST['date_soin_' + str(soin_num)]
                vetId = request.POST['soin_veto_name_' + str(soin_num)]
                soin = Soin()
                soin.date_action = datetime.datetime.strptime(dateSoin, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
                soin.veterinaire = Veterinaire.objects.get(pk=vetId)
                soin.description = request.POST['desc_soin_' + str(soin_num)]
                soin.chat = chat
                soin.num_soin = soin_num
                soin.save()
                soin_num = soin_num + 1
            return HttpResponseRedirect('/chat/display/fa')
    else:
        form = createChatForm()

    veto = Veterinaire.objects.all()
    return render(request, 'chat/create.html', {'form':form, 'veto':veto})

def displayChatAction(request, id):
    if id == -1:
        return displayChatActionBoolean(request, False)
    else:
        try:
            chat = Chat.objects.get(pk=id)
            vaccins = Vaccin.objects.all().filter(chat=chat)
            soins = Soin.objects.all().filter(chat=chat)
            actions = Action.objects.all().filter(chat=chat)
            return render(request, 'chat/display_unique.html', \
                          {'chat': chat, 'vaccins': vaccins, 'soins': soins, 'actions': actions})
        except (Chat.DoesNotExist):
            return render(request, '404.html')

def displayChatAdopteAction(request):
    return displayChatActionBoolean(request, True)

def displayChatActionBoolean(request, status):
    chats = Chat.objects.all().filter(adopte=status).order_by('entree_le')
    listVaccins = {}
    for chat in chats:
        vaccin = Vaccin.objects.all().filter(chat=chat).aggregate(Max('date_action'))
        listVaccins[chat.prenom] = vaccin
    return render(request, 'chat/display.html', {'chats': chats, 'vaccins': listVaccins, 'quantity': chats.__len__()})

def updateChatAction(request, id):
    if request.method == 'POST':
        form = createChatForm(request.POST)
        if form.is_valid():
            chat = Chat.objects.get(pk=id)
            chat.prenom = request.POST['prenom']
            chat.sexe = request.POST['sexe']
            dateNaissance = request.POST['date_naissance']
            chat.date_naissance = datetime.datetime.strptime(dateNaissance, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
            entreeLe = request.POST['entree_le']
            chat.entree_le = datetime.datetime.strptime(entreeLe, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
            if 'sterilise' in request.POST:
                chat.sterilise = True
                date_steri = request.POST['date_steri']
                chat.date_steri = datetime.datetime.strptime(date_steri, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
            else:
                chat.sterilise = False
            chat.test_vih = request.POST['test_vih']
            if chat.test_vih != "0":
                date_test_vih = request.POST['date_test_vih']
                chat.date_test_vih = datetime.datetime.strptime(date_test_vih, "%d/%m/%Y")\
                    .strftime("%Y-%m-%d")
            if 'adopte' in request.POST:
                chat.adopte = True
            else:
                chat.adopte = False
            chat.nom_fa = request.POST['nom_fa']
            chat.nom_adoptant = request.POST['nom_adoptant']
            chat.commentaire = request.POST['commentaire']
            chat.identification = request.POST['identification']
            if chat.identification == '2':
                chat.n_puce = request.POST['number']
            elif chat.identification == '3':
                chat.n_tatouage = request.POST['number']
            chat.save()
            soin_num = 0
            vaccin_num = 0
            while 'date_vaccin_' + str(vaccin_num) in request.POST:
                if 'vaccin_' + str(vaccin_num) in request.POST:
                    vaccin_num = vaccin_num + 1
                else:
                    dateVaccin = request.POST['date_vaccin_' + str(vaccin_num)]
                    vaccin = Vaccin()
                    vaccin.date_action = datetime.datetime.strptime(dateVaccin, "%d/%m/%Y") \
                        .strftime("%Y-%m-%d")
                    vetId = request.POST['vaccin_veto_name_' + str(vaccin_num)]
                    vaccin.veterinaire = Veterinaire.objects.get(pk=vetId)
                    vaccin.chat = chat
                    vaccin.type_vaccin = vaccin_num
                    vaccin.save()
                    vaccin_num = vaccin_num + 1
            while 'date_soin_' + str(soin_num) in request.POST:
                if 'soin_' + str(soin_num) in request.POST:
                    soin_num = soin_num + 1
                else:
                    dateSoin = request.POST['date_soin_' + str(soin_num)]
                    vetId = request.POST['soin_veto_name_' + str(soin_num)]
                    soin = Soin()
                    soin.date_action = datetime.datetime.strptime(dateSoin, "%d/%m/%Y") \
                        .strftime("%Y-%m-%d")
                    soin.veterinaire = Veterinaire.objects.get(pk=vetId)
                    soin.description = request.POST['desc_soin_' + str(soin_num)]
                    soin.chat = chat
                    soin.num_soin = soin_num
                    soin.save()
                    soin_num = soin_num + 1
            return HttpResponseRedirect('/chat/display/fa')
    else:
        veto = Veterinaire.objects.all()
        chat = Chat.objects.get(pk=id)
        vaccins = Vaccin.objects.all().filter(chat=chat)
        soins = Soin.objects.all().filter(chat=chat)
        numIdentification = None
        if chat.identification == 2:
            numIdentification = chat.n_puce
        if chat.identification == 3:
            numIdentification = chat.n_tatouage
        form = createChatForm(initial=
                {'prenom':chat.prenom, 'sexe':chat.sexe,
                  'date_naissance':chat.date_naissance.strftime("%d/%m/%Y"),
                  'entree_le':chat.entree_le.strftime("%d/%m/%Y"),
                  'identification':chat.identification,
                  'sterilise':chat.sterilise, 'test_vih':chat.test_vih,
                  'adopte':chat.adopte, 'nom_fa':chat.nom_fa,
                  'nom_adoptant':chat.nom_adoptant, 'commentaire':chat.commentaire,
                  'number': numIdentification,
                  'date_steri':chat.date_steri.strftime("%d/%m/%Y") if chat.date_steri != None else "",
                  'date_test_vih':chat.date_test_vih.strftime("%d/%m/%Y") if chat.date_test_vih != None else "" })
        return render(request, 'chat/edit.html', {'veto': veto, 'form': form, 'vaccins': vaccins, 'soins':soins, 'id': chat.id_chat})

def deleteChatAction(request, id):
    chat = Chat.objects.get(pk=id)
    if chat.adopte == True:
        temp = 'adopte'
    else:
        temp = 'fa'
    chat.delete()
    return HttpResponseRedirect('/chat/display/' + temp)


def adopteChatAction(request, id):
    chat = Chat.objects.get(pk=id)
    chat.adopte = True
    chat.save()
    return HttpResponseRedirect('/chat/display/fa')
