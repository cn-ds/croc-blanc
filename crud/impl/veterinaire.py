from django.shortcuts import render
from crud.forms import createVeterinaireForm
from django.http import HttpResponseRedirect
from crud.models import Chat, Action, Vaccin, Soin, Veterinaire

def createVeterinaireAction(request):
    if request.method == 'POST':
        form = createVeterinaireForm(request.POST)
        if form.is_valid():
            veto = form.save()
            return HttpResponseRedirect('/veto/display/')
    else:
        form = createVeterinaireForm()
    return render(request, 'veto/create.html', {'form':form})

def displayVeterinaireAction(request, id):
    if id == -1:
        veto = Veterinaire.objects.all()
        return render(request, 'veto/display.html', {'veto': veto})
    else:
        try:
            veto = Veterinaire.objects.get(pk=id)
            return render(request, 'veto/display_unique.html', {'veto': veto})
        except (Veterinaire.DoesNotExist):
            return render(request, '404.html')

def updateVeterinaireAction(request, id):
    if request.method == 'POST':
        form = createVeterinaireForm(request.POST)
        if form.is_valid():
            veto = Veterinaire.objects.get(pk=id)
            veto.nom_vet = request.POST['nom_vet']
            veto.address_1 = request.POST['address_1']
            veto.address_2 = request.POST['address_2']
            veto.ville = request.POST['ville']
            veto.numero_tel = request.POST['numero_tel']
            veto.save()
            return HttpResponseRedirect('/veto/display/')
    else:
        veto = Veterinaire.objects.get(pk=id)
        form = createVeterinaireForm(initial=
                {'nom_vet':veto.nom_vet, 'address_1':veto.address_1,
                    'address_2':veto.address_2, 'ville':veto.ville,
                    'numero_tel':veto.numero_tel})
        return render(request, 'veto/edit.html', {'form': form, 'id': veto.id_vet})

def deleteVeterinaireAction(request, id):
    veto = Veterinaire.objects.get(pk=id)
    veto.delete()
    return HttpResponseRedirect('/veto/display/')





