# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.http import HttpResponse
from crud.impl.veterinaire import *
from crud.impl.chat import *
from crud.models import User, Chat, Action, Vaccin, Soin, Veterinaire
from django.core.mail import send_mail
from datetime import timedelta, date as datef
import logging
import datetime
from urllib.parse import urlencode

logger = logging.getLogger('croc_blanc.log')

# Show log of actions
def printInfoLog(message, request):
    idUser = 'none'
    if 'id_user' in request.session:
        idUser = request.session['id_user'].__str__()
    dayDate = datetime.datetime.now()
    logger.info(dayDate.__str__() + '|' +  idUser + '|' + message + "|" + request.method.__str__() + "|" \
                + request.path + "|" + urlencode(request.POST) + "|" + urlencode(request.GET) + ">")

# Util function used to check if the user is authenticated or not
def isAuthenticated(request):
    if 'connected' in request.session:
        if request.session['connected'] == True:
            return True
    return False


# Create your views here.

def connect(request):
    printInfoLog("Login attempt",request)
    if request.method == 'POST':
        try:
            user = User.objects.get(email=request.POST['login'])
            if user.password == request.POST['password']:
                printInfoLog("Login successful", request)
                request.session['connected'] = True
                request.session['id_user'] = user.id_user
                return displayChat(request)
            else:
                request.session['connected'] = False
                request.session['id_user'] = 0
                return render(request, 'login.html')
        except (User.DoesNotExist):
            request.session['connected'] = False
            request.session['id_user'] = 0
            return render(request, 'login.html')
    else:
        return render(request, 'login.html')


def disconnect(request):
    if 'connected' in request.session:
        del request.session['connected']
    return connect(request)


def index(request):
    printInfoLog("Index", request)
    if isAuthenticated(request):
        return render(request, 'index.html')
    else:
        return redirect('/connection')


def updateVeterinaire(request, id):
    printInfoLog("Update Veterinaire", request)
    if isAuthenticated(request):
        return updateVeterinaireAction(request, id)
    else:
        return redirect('/connection')


def deleteVeterinaire(request, id):
    printInfoLog("Delete Veterinaire", request)
    if isAuthenticated(request):
        return deleteVeterinaireAction(request, id)
    else:
        return redirect('/connection')


def displayVeterinaire(request, id=-1):
    printInfoLog("Display veterinaire: " + id.__str__(), request)
    if isAuthenticated(request):
        return displayVeterinaireAction(request, id)
    else:
        return redirect('/connection')


def createVeterinaire(request):
    printInfoLog("Create Veterinaire", request)
    if isAuthenticated(request):
        return createVeterinaireAction(request)
    else:
        return redirect('/connection')


def createChat(request):
    printInfoLog("Create Chat", request)
    if isAuthenticated(request):
        return createChatAction(request)
    else:
        return redirect('/connection')


def displayChat(request, id=-1):
    printInfoLog("Display Chat", request)
    if isAuthenticated(request):
        return displayChatAction(request, id)
    else:
        return redirect('/connection')


def updateChat(request, id):
    printInfoLog("Update Chat", request)
    if isAuthenticated(request):
        return updateChatAction(request, id)
    else:
        return redirect('/connection')


def deleteChat(request, id):
    printInfoLog("Delete Chat", request)
    if isAuthenticated(request):
        return deleteChatAction(request, id)
    else:
        return redirect('/connection')


def adopteChat(request, id):
    printInfoLog("Adopte Chat", request)
    if isAuthenticated(request):
        return adopteChatAction(request, id)
    return redirect('/connection')


def email(request):
    chats = Chat.objects.all().filter(adopte=False)
    primoChat = []
    rappelChat = []
    premierRappelChat = []
    steriChat = []
    for chat in chats:
        vaccin = Vaccin.objects.filter(chat=chat).annotate(max_date=Max('date_action'))
        if len(vaccin) > 0:
            # If only one vaccine was done
            if len(vaccin)==1 and vaccin[0].type_vaccin == 0:
                date = timedelta(days=21)
                if vaccin[0].date_action + date < datetime.datetime.now().date():
                    premierRappelChat.append(chat)
            # If more than one vaccin was done
            if vaccin[len(vaccin)-1].type_vaccin > 0:
                date = timedelta(days=330)
                if vaccin[0].date_action + date < datetime.datetime.now().date():
                    rappelChat.append(chat)
        else:
            # If the cat wasn't vaccinated
            date = timedelta(days=15)
            if chat.entree_le + date < datetime.datetime.now().date():
                primoChat.append(chat)
        # If the cat is 6 month old we need to sterilize it
        date = timedelta(days=180)
        if chat.sterilise == False and chat.date_naissance + date < datetime.datetime.now().date():
            steriChat.append(chat)

    textMessage = render_to_string('email.html', {
        'chatPrimo': primoChat,
        'chatPremierRappel': premierRappelChat,
        'chatRappel': rappelChat,
        'chatCastration': steriChat
    })

    today = datef.today()

    send_mail(
        'Croc blanc - ' + today.day.__str__() + '/' + today.month.__str__() + '/' + today.year.__str__(),
        textMessage,
        'no-reply@croc-blanc.fr',
        ['***REMOVED***'],
        fail_silently=False,
        auth_user=None,
        auth_password=None,
        connection=None,
        html_message=textMessage
    )
    return HttpResponse("OK")
