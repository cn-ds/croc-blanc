from django import template

register = template.Library()

@register.filter
def telephoneFormat(value):
    return " ".join(value[i:i + 2] for i in range(0, len(value), 2))

@register.filter
def identifiantFormat(value):
    return " ".join(value[i:i + 3] for i in range(0, len(value), 3))