from django import template

register = template.Library()

@register.filter
def get_value_from_dict(dict, key):

    """
    usage example {{ your_dict|get_value_from_dict:your_key }}
    """
    if key:
        if dict[key]["date_action__max"] != None:
            return dict[key]["date_action__max"]
    return " "
