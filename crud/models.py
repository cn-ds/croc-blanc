#-*- coding: utf-8 -*-
from django.db import models

class User(models.Model):
    id_user = models.AutoField(primary_key=True)
    email = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)

class Chat(models.Model):
    id_chat         = models.AutoField(primary_key=True)
    prenom          = models.CharField(max_length=50)
    sexe            = models.BooleanField()
    date_naissance  = models.DateField()
    entree_le       = models.DateField()
    identification  = models.IntegerField()
    sterilise       = models.BooleanField()
    date_steri      = models.DateField(blank=True, null=True)
    test_vih        = models.IntegerField()
    date_test_vih   = models.DateField(blank=True, null=True)
    adopte          = models.BooleanField()
    nom_fa          = models.CharField(max_length=50, blank=True, null=True)
    nom_adoptant    = models.CharField(max_length=50, blank=True, null=True)
    commentaire     = models.TextField(blank=True, null=True)
    n_puce          = models.CharField(max_length=100, blank=True, null=True)
    n_tatouage      = models.CharField(max_length=100, blank=True, null=True)

class Veterinaire(models.Model):
    id_vet = models.AutoField(primary_key=True)
    nom_vet = models.CharField(max_length=50)
    address_1 = models.CharField(max_length=255, blank=True, null=True)
    address_2 = models.CharField(max_length=255, blank=True, null=True)
    ville = models.CharField(max_length=50, blank=True, null=True)
    numero_tel = models.CharField(max_length=10, blank=True, null=True)

#Abstract class for all the actions possible on a cat
class Action(models.Model):
    id_action = models.AutoField(primary_key=True)
    date_action = models.DateField()
    veterinaire = models.ForeignKey(Veterinaire)
    chat = models.ForeignKey(Chat)

class Vaccin(Action):
    type_vaccin = models.IntegerField()

class Soin(Action):
    description = models.TextField()
    num_soin = models.IntegerField()
