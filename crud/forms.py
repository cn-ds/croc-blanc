from django import forms
from django.forms import ModelForm
from crud.models import Chat, Action, Vaccin, Soin, Veterinaire

IDENTIFICATION = [(1, 'Pas encore faite'), (2, 'Puce'), (3, 'Tatouage'),]
VIH = [(0, 'Pas encore fait'), (1, 'Positif'), (2, 'Négatif'),]
SEXE = [(True, 'Male'), (False, 'Femelle'),]

class createVeterinaireForm(ModelForm):
    class Meta:
        model = Veterinaire
        exclude = ['id_vet']

class createChatForm(ModelForm):
    identification = forms.ChoiceField(required=True, choices=IDENTIFICATION)
    test_vih = forms.ChoiceField(required=True, choices=VIH)
    sexe = forms.ChoiceField(required=True, choices=SEXE)
    date_naissance = forms.CharField(required=True)
    entree_le = forms.CharField(required=True)
    number = forms.CharField(required=False)
    date_steri = forms.CharField(required=False)
    date_test_vih = forms.CharField(required=False)
    class Meta:
        model = Chat
        exclude = ['id_chat', 'identification', 'test_vih', 'sexe', 'date_naissance', 'entree_le', \
                   'n_puce', 'n_tatouage', 'date_test_vih', 'date_steri']
