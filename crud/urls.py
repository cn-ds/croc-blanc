from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
    #VETO
	url(r'^veto/create/$', views.createVeterinaire, name='veto-create'),
	url(r'^veto/update/(?P<id>[0-9]+)/$', views.updateVeterinaire, name='veto-update'),
	url(r'^veto/delete/(?P<id>[0-9]+)/$', views.deleteVeterinaire, name='veto-delete'),
	url(r'^veto/display/$', views.displayVeterinaire, name='veto-display'),
	url(r'^veto/display/(?P<id>[0-9]+)/$', views.displayVeterinaire, name='veto-display'),
    #CHAT
    url(r'^chat/create/$', views.createChat, name='chat-create'),
    url(r'^chat/display/adopte$', views.displayChatAdopteAction, name='chat-display-adopte'),
    url(r'^chat/display/fa$', views.displayChat, name='chat-display-fa'),
    url(r'^chat/delete/(?P<id>[0-9]+)/$', views.deleteChat, name='chat-delete'),
    url(r'^chat/display/(?P<id>[0-9]+)/$', views.displayChat, name='chat-display'),
    url(r'^chat/update/(?P<id>[0-9]+)/$', views.updateChat, name='chat-update'),
    url(r'^chat/adopte/(?P<id>[0-9]+)/$', views.adopteChat, name='chat-adopte'),
    #CONNECTION
    url(r'^connection/', views.connect, name='connect'),
	url(r'^disconnect/', views.disconnect, name='disconnect'),
    #EMAIL
    url(r'^chat/email', views.email, name='email'),
]
