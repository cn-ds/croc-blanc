# Gestion des soins vétérinaires

## Introduction

Application permettant de gérer les soins vétérinaires de l'association [Croc blanc](http://www.crocblanc.org/). Il s'agit d'un site qui n'est accessible qu'aux bénévoles de l'association et permet de suivre un chat pendant toute la période qu'il passe dans une des familles d'accueil de l'association.

L'application répond aux besoins de suivi d'une association de protection animale. Elle permet aussi de respecter certaines obligations légales sur la traçabilité des chats.

## Gains

Les gains sont multiples, meilleur suivi des chats, réduction des coûts associés aux frais engendrés pas les vaccins manqués qui doivent être refaits et une meilleure communication entre les différents bénévoles.

## Fonctionnalités

L'application gère un chat de la naissance ou son recueil jusqu'à son adoption.

* Gestion de la vaccination
* Suivi des soins vétérinaires
* Suivi des familles d'accueil
* Envoi de rappel par mail pour ne pas rater les soins à faire sur les chats (ex : rappels de vaccins)

## Quelques captures

![Page d'acceuil](./.readme_img/image_1.png)
![Fiche d'un chat](./.readme_img/image_2.png)
![Edition d'un chat](./.readme_img/image_3.png)
![Liste des vétérinaires](./.readme_img/image_4.png)

## Installation & déploiement

Les informations sont sur le site de [Django](https://docs.djangoproject.com/en/2.1/intro/tutorial01/)

Pré-requis :
* Python 3
* Django 1.10.1
* Venv
* Gunicorn

``` shell
python3 manage.py collectstatic # Generate static files
gunicorn --bind 0.0.0.0:8000 croc_blanc.wsgi:application
```

# Requete SQL pour l'export

``` sql
SELECT id_chat, prenom, 
	CASE sexe
     WHEN 1 THEN 'Male'
     WHEN 0 THEN 'Femelle'
	END as sexe, date_naissance, entree_le,
	CASE sterilise
     WHEN 1 THEN 'Oui'
     WHEN 0 THEN 'Non'
	END as sterilise, 
	CASE test_vih
     WHEN 1 THEN 'Positif'
     WHEN 0 THEN 'Non fait'
	 WHEN 2 THEN 'Négatif'
	END as test_vih, 
    CASE adopte
     WHEN 1 THEN 'Adopté'
     WHEN 0 THEN 'En FA'
    END as adopte, nom_fa,
	nom_adoptant, commentaire, n_puce, n_tatouage, date_steri, date_test_vih
 FROM crud_chat WHERE entree_le > '2018-12-31' and entree_le < '2020-01-01';
```

